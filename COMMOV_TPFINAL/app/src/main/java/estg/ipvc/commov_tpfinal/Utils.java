package estg.ipvc.commov_tpfinal;

/**
 * Created by marcio on 02/04/2017.
 */

public class Utils {

    public static final String param_status = "status";
    public static final String EMAIL = "email";
    public static final String param_dados = "results";
    public static final String param_nome = "nome";
    public static final String param_email = "email";
    public static final String output_erro = "ERRO";
    public static final String msg = "msg";
}
