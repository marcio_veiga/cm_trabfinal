package estg.ipvc.commov_tpfinal.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.BreakIterator;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import estg.ipvc.commov_tpfinal.MySingleton;
import estg.ipvc.commov_tpfinal.R;
import estg.ipvc.commov_tpfinal.Utils;
import estg.ipvc.commov_tpfinal.PermissionUtils;

/**
 * Created by marcio on 14/04/2017.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    MapView mMapView;
    GoogleMap googleMap;
    LocationRequest mLocationRequest;

    //adicionar multiplos pontos ao mapa
    private MarkerOptions options = new MarkerOptions();

    private GoogleApiClient mGoogleApiClient;


    //update localização
    //true pro defeito mas depende se o utilizador autoriza ou nao meter no metodo correcto
    private boolean mRequestingLocationUpdates = true;

    private TextView mLatitudeTextView;
    private TextView mLongitudeTextView;
    private TextView mLastUpdateTimeTextView;
    private Location mCurrentLocation;
    private String mLastUpdateTime;
    private Location mLastLocation;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately


        mLastUpdateTimeTextView = (TextView) rootView.findViewById(R.id.mLastUpdateTime);
        mLatitudeTextView = (TextView) rootView.findViewById(R.id.mLatitude);
        mLongitudeTextView = (TextView) rootView.findViewById(R.id.mLongitude);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        createLocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());



        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {

                googleMap = mMap;

                enableMyLocation();

                //adicionar marker
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(41.1, -8.2))
                        .title("Marker"));


                addPontosProximos(mMap);

                // definir novo ponto
                LatLng sydney = new LatLng(40.689247, -74.044502);

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 18));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(sydney)
                        .zoom(1)
                        .bearing(45)
                        .tilt(60)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        return false;
                    }
                });
            }

        });

        return rootView;
    }

    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    //definir o intervalo de verificação da posição do dispotisivo
    protected void createLocationRequest() {
        Log.d("TAG", "ENTRA createLocationRequest");

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

    }

    public void enableMyLocation(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(getActivity(), 1,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void addPontosProximos(final GoogleMap mMap) {

        //WEB SERVICE PEDIDO DOS PONTOS
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=AIzaSyDmnW7cVyFiVLI9BmHX1MwhVY_xqfBruxc";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>(){

                    public void onResponse(JSONObject response) {
                        try {

                            JSONArray arr = response.getJSONArray(Utils.param_dados);

                            for(int i = 0; i < arr.length(); i++){

                                JSONObject obj = arr.getJSONObject(i);


                              //  Toast.makeText(getActivity(), obj.getString("geometry"), Toast.LENGTH_SHORT).show();

                                //Log.d("TAG", "3" + obj.getJSONObject("geometry").getJSONObject("location").getString("lat"));

                                String tempLat = obj.getJSONObject("geometry").getJSONObject("location").getString("lat");
                                String tempLng = obj.getJSONObject("geometry").getJSONObject("location").getString("lng");
                                String tempTitle = obj.getString("name");
                                LatLng temp = new LatLng(Double.valueOf(tempLat), Double.valueOf(tempLng));
                                options.position(temp);
                                //options.title(tempTitle);
                                options.snippet("someDesc");

                                mMap.addMarker(options);

                            //    latlngs.add(new LatLng(Double.valueOf(tempLat), Double.valueOf(tempLng)));

                            }
/*


                            for (LatLng point : latlngs) {

                             //   Toast.makeText(getActivity(), "ENTRA 2", Toast.LENGTH_SHORT).show();


                                options.position(point);
                                options.title("someTitle");
                                options.snippet("someDesc");
                                mMap.addMarker(options);
                            }
*/
                        } catch(JSONException ex){
                            Log.d("ERRO",ex.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    public void onErrorResponse(VolleyError error){
                        ///  ((TextView) findViewById(R.id.texto)).setText(Utils.output_erro);
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {}

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

        Toast.makeText(getContext(), "Lat: " + String.valueOf(latLng.latitude)
                + "Long: " + String.valueOf(latLng.longitude), Toast.LENGTH_SHORT).show();

        googleMap.addMarker(new MarkerOptions()
                .position(latLng));
    }


    //executado onReady do fragment, vai retornar a ultima localização do dispositivo
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d("TAG", "entra onConnected");

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            mLatitudeTextView.setText(String.valueOf(mLastLocation.getLatitude()));
            mLongitudeTextView.setText(String.valueOf(mLastLocation.getLongitude()));
        }

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    protected void startLocationUpdates() {
        Log.d("TAG", "ENTRA2");
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.d("TAG", "ERRO entrou em onConnectionFailed");
        // An unresolvable error has occurred and a connection to Google APIs
        // could not be established. Display an error message, or handle
        // the failure silently

        // ...
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("TAG", "LOC MUDOU3");
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateUI();
    }

    private void updateUI() {
        mLatitudeTextView.setText(String.valueOf(mCurrentLocation.getLatitude()));
        mLongitudeTextView.setText(String.valueOf(mCurrentLocation.getLongitude()));
        mLastUpdateTimeTextView.setText(mLastUpdateTime);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }



}