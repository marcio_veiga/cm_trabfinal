package estg.ipvc.commov_tpfinal.db;

import android.provider.BaseColumns;

import org.w3c.dom.Text;

/**
 * Created by marcio on 08/04/2017.
 */
// FALTA VERIFICAR SE EXISTE O DATE TYPE OU SE É PARA USAR TEXT_TYPE


public class Contrato {
    private static final String TEXT_TYPE = " TEXT ";
    private static final String INT_TYPE = " INTEGER ";

    public Contrato(){

    }
    //REPRESENTA A ESTRUTURA QUE A TABELA VAI TER
    public static abstract class Local implements BaseColumns {
        public static final String TABLE_NAME = "local";
        public static final String COLUMN_IDUTILIZADOR = "idutilizador";
        public static final String COLUMN_NOME = "nome";
        public static final String COLUMN_MORADA = "morada";
        public static final String COLUMN_CONTATO = "contato";
        public static final String COLUMN_RATING = "rating";
        public static final String COLUMN_DATAADICIONADO = "dataadicionado";
        public static final String COLUMN_DESCRICAO = "descricao";
        public static final String COLUMN_IDTIPO = "idtipo";

        public static String[] PROJECTION = {Local._ID, Local.COLUMN_IDUTILIZADOR, Local.COLUMN_NOME,
                                                        Local.COLUMN_MORADA, Local.COLUMN_CONTATO,
                                                        Local.COLUMN_RATING, Local.COLUMN_DATAADICIONADO,
                                                        Local.COLUMN_DESCRICAO, Local.COLUMN_IDTIPO};

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + Local.TABLE_NAME + "(" +
                        Local._ID + INT_TYPE + " PRIMARY KEY," +
                        Local.COLUMN_IDUTILIZADOR + INT_TYPE + "," +
                        Local.COLUMN_NOME + TEXT_TYPE + "," +
                        Local.COLUMN_MORADA + TEXT_TYPE + "," +
                        Local.COLUMN_CONTATO + TEXT_TYPE + "," +
                        Local.COLUMN_RATING + INT_TYPE + "," +
                        Local.COLUMN_DATAADICIONADO + TEXT_TYPE + "," +
                        Local.COLUMN_DESCRICAO + TEXT_TYPE + "," +
                        Local.COLUMN_IDTIPO + INT_TYPE + " REFERENCES " +
                        Tipo.TABLE_NAME + "(" + Tipo._ID + "));";

        public static final String SQL_DROP_ENTRIES =
                "DROP TABLE " + Local.TABLE_NAME + ";";
    }

    public static abstract class Tipo implements BaseColumns{
        public static final String TABLE_NAME = "cidade";
        public static final String COLUMN_DESCRICAO = "Tdescricao";

        public static String[] PROJECTION = {Tipo._ID, Tipo.COLUMN_DESCRICAO};

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + Tipo.TABLE_NAME + "(" +
                        Tipo._ID + INT_TYPE + " PRIMARY KEY," +
                        Tipo.COLUMN_DESCRICAO + TEXT_TYPE + ");";

        public static final String SQL_DROP_ENTRIES =
                "DROP TABLE " + Tipo.TABLE_NAME + ";";
    }


}

