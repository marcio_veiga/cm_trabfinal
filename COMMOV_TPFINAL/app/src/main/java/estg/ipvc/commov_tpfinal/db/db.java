package estg.ipvc.commov_tpfinal.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by marcio on 08/04/2017.
 */

public class db extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "pessoas.db";

    public db(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Contrato.Tipo.SQL_CREATE_ENTRIES);
        db.execSQL(Contrato.Local.SQL_CREATE_ENTRIES);

        db.execSQL("INSERT INTO "+ Contrato.Tipo.TABLE_NAME + " VALUES(1, 'Tipo1'); ");
        db.execSQL("INSERT INTO "+ Contrato.Tipo.TABLE_NAME + " VALUES(2, 'Tipo2'); ");
        db.execSQL("INSERT INTO "+ Contrato.Tipo.TABLE_NAME + " VALUES(3, 'Tipo3'); ");

        db.execSQL("INSERT INTO " + Contrato.Local.TABLE_NAME + " VALUES(1, 1, 'Torre Eiffel', 'Moledo', 32412, 5, 'data', 'descricao', 1 );");

    }
    //apagamos e recriamos a bd com os novos dados;
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Contrato.Local.SQL_DROP_ENTRIES);
        db.execSQL(Contrato.Tipo.SQL_DROP_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
