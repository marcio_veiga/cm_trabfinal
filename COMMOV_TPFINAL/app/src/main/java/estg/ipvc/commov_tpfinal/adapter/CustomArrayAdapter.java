package estg.ipvc.commov_tpfinal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import estg.ipvc.commov_tpfinal.R;
import estg.ipvc.commov_tpfinal.entities.Place;
import estg.ipvc.commov_tpfinal.fragments.ListPlacesFragment;

/**
 * Created by marcio on 14/04/2017.
 */

public class CustomArrayAdapter extends ArrayAdapter<Place> {

    public CustomArrayAdapter(Context context, ArrayList<Place> places){
        super(context, 0, places);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Place p = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_places, parent, false);
        }

      //  ((TextView) convertView.findViewById(R.id.textView1)).setText(p.getExemploParam1());

        return convertView;
    }
}
