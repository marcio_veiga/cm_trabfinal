package estg.ipvc.commov_tpfinal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import estg.ipvc.commov_tpfinal.fragments.TabsFragment;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity
                            implements NavigationView.OnNavigationItemSelectedListener {

    private TextView navHeaderName;
    private TextView navHeaderEmail;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

     //   navHeaderEmail = (TextView)findViewById(R.id.navHeaderEmail);
     //   navHeaderName = (TextView)findViewById(R.id.navHeaderNome);

        //inicializar sharedPreferences
        android.content.SharedPreferences sharedPref = getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
        String email = sharedPref.getString(Utils.EMAIL, null);

//        navHeaderName.setText("teste");
 //       navHeaderEmail.setText("teste");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, new TabsFragment())
                .commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_everything:
                Toast.makeText(this, "Sem filtro", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_historicalMonuments:

                break;
            case R.id.nav_artMuseums:

                break;
            case R.id.nav_parks:

                break;
            case R.id.nav_foodDrink:

                break;
            case R.id.nav_architecture:

                break;
            case R.id.nav_favPlaces:

                Intent favPlaces = new Intent(this, FavouriteActivity.class);
                startActivity(favPlaces);

                break;
            case R.id.nav_visitPlaces:

                Intent visitPlaces = new Intent(this, VisitedActivity.class);
                startActivity(visitPlaces);

                break;
            case R.id.nav_settings:

                Intent settingsAct = new Intent(this, VisitedActivity.class);
                startActivity(settingsAct);

                break;
            case R.id.nav_logout:
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove("email");
                editor.commit();


                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            default:

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
