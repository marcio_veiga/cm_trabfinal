package estg.ipvc.commov_tpfinal.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import estg.ipvc.commov_tpfinal.R;
import estg.ipvc.commov_tpfinal.Utils;
import estg.ipvc.commov_tpfinal.adapter.CustomArrayAdapter;
import estg.ipvc.commov_tpfinal.entities.Place;

/**
 * Created by marcio on 14/04/2017.
 */

public class ListPlacesFragment extends Fragment {

    private TextView userLogado;

    public ListPlacesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        getActivity().setTitle("Places List");

        ArrayList<Place> arrayPlace = new ArrayList<>();
        //arrayPlace.add(new Place("param1","param2","param3"));

        CustomArrayAdapter itemsAdapter =
                new CustomArrayAdapter(view.getContext(), arrayPlace);

        ((ListView) view.findViewById(R.id.listPlaces)).setAdapter(itemsAdapter);


        userLogado = (TextView) view.findViewById(R.id.userLogado);

        //inicializar sharedPreferences
        android.content.SharedPreferences sharedPref = getContext().getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
        String email = sharedPref.getString(Utils.EMAIL, null);

        userLogado.setText(email.toString());

        return view;
    }

}
