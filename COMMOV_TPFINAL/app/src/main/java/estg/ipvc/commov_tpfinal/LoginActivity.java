package estg.ipvc.commov_tpfinal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private Button btnRegisto;
    private Button btnRegistoGoogle;
    private EditText inputEmail;
    private EditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegisto = (Button) findViewById(R.id.btnRegisto);
        btnRegistoGoogle = (Button) findViewById(R.id.btnRegistoGoogle);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);

        //inicializar sharedPreferences
        android.content.SharedPreferences sharedPref = getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
        String email = sharedPref.getString(Utils.EMAIL, null);

        // se email es tiver declarado o user ja esta logado passa logo para a Main
        if(email != null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    public void efetuarLogin(View view) {

        String currentEmail = inputEmail.getText().toString();
        String currentPassword = inputPassword.getText().toString();

        if(currentEmail.isEmpty()){

            inputEmail.setError("Preenchimento Obrigatório");

        }

        if(currentPassword.isEmpty()){
            inputPassword.setError("Preenchimento Obrigatório");
        }

        if(!currentEmail.isEmpty() && !currentPassword.isEmpty()){

            processaLogin(currentEmail, currentPassword);

        }


    }

    public void processaLogin(String email, String password){

       // StringRequest sr = new StringRequest(Request.Method.POST,"http://commovtpfinal.000webhostapp.com/login.php", new Response.Listener<String>() {
        StringRequest sr = new StringRequest(Request.Method.POST,"http://192.168.137.1:8080/COMOOVtpfinal/login.php", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    //Do it with this it will work
                    JSONObject person = new JSONObject(response);
                    String status = person.getString(Utils.param_status);
                    String mensagem = person.getString(Utils.msg);

                    // Starting MainActivity

                    if (status.equals("true")){

                        //Toast.makeText(getApplicationContext(), "aceite: " + status, Toast.LENGTH_LONG).show();
                        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(Utils.EMAIL, inputEmail.getText().toString());
                        editor.commit();



                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(), "nao aceite: " + status, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("email",inputEmail.getText().toString());
                params.put("password",inputPassword.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(sr);

    }



    public void efetuarLoginGoogle(View view) {

    }

    //perceber a necessidade do uso do finish;
    public void atividadeRegisto(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }
}
