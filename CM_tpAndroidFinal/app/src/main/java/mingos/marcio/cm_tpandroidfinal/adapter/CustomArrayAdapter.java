package mingos.marcio.cm_tpandroidfinal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mingos.marcio.cm_tpandroidfinal.R;
import mingos.marcio.cm_tpandroidfinal.entities.Place;


/**
 * Created by marcio on 14/04/2017.
 */

public class CustomArrayAdapter extends ArrayAdapter<Place> {

    public CustomArrayAdapter(Context context, ArrayList<Place> places){
        super(context, 0, places);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Place place = getItem(position);


        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_places, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.nome)).setText(place.getName());
        ((TextView) convertView.findViewById(R.id.morada)).setText(place.getFormatted_address());

        return convertView;
    }
}
