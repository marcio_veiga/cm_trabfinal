package mingos.marcio.cm_tpandroidfinal;

import java.util.ArrayList;

import mingos.marcio.cm_tpandroidfinal.entities.Place;

/**
 * Created by marcio on 02/04/2017.
 */

public class Utils {

    public static final String param_status = "status";
    public static final String EMAIL = "email";
    public static final String NOME = "nome";
    public static final String param_dados = "results";
    public static final String param_nome = "nome";
    public static final String param_email = "email";
    public static final String output_erro = "ERRO";
    public static final String msg = "msg";
    public static final String RADIUS = "2000";
    public static String TYPE = null;
    public static ArrayList<Place> FOUNDED_PLACES = null;
    public static double latAtual = 0;
    public static double lngAtual = 0;

}
