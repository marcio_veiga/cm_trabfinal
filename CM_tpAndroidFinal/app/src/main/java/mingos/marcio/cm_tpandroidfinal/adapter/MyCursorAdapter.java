package mingos.marcio.cm_tpandroidfinal.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import mingos.marcio.cm_tpandroidfinal.R;
import mingos.marcio.cm_tpandroidfinal.db.Contrato;

/**
 * Created by marcio on 04/04/2017.
 */

public class MyCursorAdapter extends CursorAdapter{

    private Context mContext;
    private int id;
    private Cursor mCursor;

    public MyCursorAdapter(Context context, Cursor c) {

        super(context, c, 0);
        mContext = context;
        mCursor = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_places, parent, false);
    }

    @Override
    public void bindView(View mView, Context context, Cursor cursor) {

        TextView text1 = (TextView) mView.findViewById(R.id.nome);
        TextView text2 = (TextView) mView.findViewById(R.id.morada);

        text1.setText(mCursor.getString(cursor.getColumnIndexOrThrow(Contrato.Local.COLUMN_NOME)));
        text2.setText(mCursor.getString(cursor.getColumnIndexOrThrow(Contrato.Local.COLUMN_MORADA)));
    //    text2.setText(String.valueOf(mCursor.getInt(cursor.getColumnIndexOrThrow(Contrato.P essoa.COLUMN_IDADE))));

    }
}
