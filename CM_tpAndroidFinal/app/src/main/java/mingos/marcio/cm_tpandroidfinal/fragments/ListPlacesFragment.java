package mingos.marcio.cm_tpandroidfinal.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mingos.marcio.cm_tpandroidfinal.R;
import mingos.marcio.cm_tpandroidfinal.Utils;
import mingos.marcio.cm_tpandroidfinal.adapter.CustomArrayAdapter;
import mingos.marcio.cm_tpandroidfinal.entities.Place;

import static android.R.id.list;


/**
 * Created by marcio on 14/04/2017.
 */

public class ListPlacesFragment extends Fragment{

    private TextView userLogado;
    Place testePlace;
    private MapFragment places;
    ArrayList<Place> foundedPlaces;

    public ListPlacesFragment() {
        places = new MapFragment();
        foundedPlaces = places.getPlacesFounded();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Toast.makeText(getContext(), "TESTE LISTA: "+ foundedPlaces.size() , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Toast.makeText(getContext(), "entra onResume", Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        getActivity().setTitle("Places List");

        userLogado = (TextView) view.findViewById(R.id.userLogado);

        //inicializar sharedPreferences
        android.content.SharedPreferences sharedPref = getContext().getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
        String email = sharedPref.getString(Utils.EMAIL, null);

//        Toast.makeText(getContext(), "TESTE LISTA: "+ foundedPlaces.size() , Toast.LENGTH_SHORT).show();
        if(email != null){
            userLogado.setText(email.toString());
        }




        ArrayList<Place> arrayPlace = new ArrayList<>();
       // arrayPlace.add(new Place("id","nome", "address", "icon", "rating"));
       // arrayPlace.add(new Place("id","nome", "address", "icon", "rating"));
       // arrayPlace.add(new Place("id","nome", "address", "icon", "rating"));

         CustomArrayAdapter itemsAdapter =
               new CustomArrayAdapter(view.getContext(), arrayPlace);

             ((ListView) view.findViewById(R.id.listPlaces)).setAdapter(itemsAdapter);


        return view;
    }



    public void fillLista() {
    }
}
