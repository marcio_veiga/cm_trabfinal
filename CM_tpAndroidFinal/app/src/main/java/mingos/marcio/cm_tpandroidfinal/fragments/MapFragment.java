package mingos.marcio.cm_tpandroidfinal.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mingos.marcio.cm_tpandroidfinal.MySingleton;
import mingos.marcio.cm_tpandroidfinal.PlaceDetailsActivity;
import mingos.marcio.cm_tpandroidfinal.R;
import mingos.marcio.cm_tpandroidfinal.Utils;
import mingos.marcio.cm_tpandroidfinal.entities.Place;


public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationSource.OnLocationChangedListener, LocationListener, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private static final int REQUEST_CODE = 0;

    MapView mMapView;
    private GoogleMap googleMap;
    GoogleApiClient googleApiClient;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;

    private String radius = "2000";
    private double latAtual;
    private double lngAtual;

    private  ArrayList<Place> placesFounded;



    public MapFragment() {
        // Required empty public constructor
    }

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        placesFounded = new ArrayList<>();

        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(this);

        FloatingActionButton openDetailsPlace = (FloatingActionButton) rootView.findViewById(R.id.setRadiusSearch);

        openDetailsPlace.setOnClickListener(this);

        return rootView;

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(getContext(), "ENTROU", Toast.LENGTH_SHORT).show();
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getContext())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        99);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        99);
            }
        }
    }

    //executado ao carregar o mapa com sucesso
    @Override
    public void onMapReady(GoogleMap map) {

        googleMap = map;

        googleMap.setOnMyLocationButtonClickListener(this);
        googleMap.setOnMarkerClickListener(this);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                googleMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            googleMap.setMyLocationEnabled(true);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("ENTRA", "ENTRA");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(200);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("TAG", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("TAG", "connection failed");
    }


    @Override
    public void onLocationChanged(Location location) {

        googleMap.clear();
        placesFounded.clear();

        Log.d("TESTE", "LOCALIZACAO MUDOU");

        mLastLocation = location;

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

         latAtual = location.getLatitude();
         lngAtual = location.getLongitude();

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = googleMap.addMarker(markerOptions);

        //move map camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 1));

        //Agora aqui fazemos o pedido de webService para mostrar os places proximo da localização atual
        addPontosProximos(latAtual, lngAtual);
    }

    //WEB SERVICE PEDIDO DOS PONTOS
    public void addPontosProximos(double latAtual, double lngAtual) {


        LatLng tempLatlng = new LatLng(Double.valueOf(latAtual),Double.valueOf(lngAtual));

        googleMap.addCircle(new CircleOptions()
                    .center(tempLatlng)
                    .radius(Double.valueOf(radius))
                    .strokeColor(R.color.colorPrimaryGreen)
                    .fillColor(R.color.radiusTint));


        String localizacaoAtual = String.valueOf(latAtual) + "," + String.valueOf(lngAtual);
        String url;

        if(Utils.TYPE != null){

            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + localizacaoAtual + "&radius=" + radius + "&type=" + Utils.TYPE + "&key=AIzaSyDmnW7cVyFiVLI9BmHX1MwhVY_xqfBruxc";

        }else{
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + localizacaoAtual + "&radius=" + radius + "&key=AIzaSyDmnW7cVyFiVLI9BmHX1MwhVY_xqfBruxc";

        }



        //String url = "http://192.168.137.1:8080/COMOOVtpfinal/googlePlaces.json";

        Log.d("TAG", url);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    public void onResponse(JSONObject response) {
                        try {

                            JSONArray arr = response.getJSONArray(Utils.param_dados);

                            for (int i = 0; i < arr.length(); i++) {

                                JSONObject obj = arr.getJSONObject(i);

                                String tempLat = obj.getJSONObject("geometry").getJSONObject("location").getString("lat");
                                String tempLng = obj.getJSONObject("geometry").getJSONObject("location").getString("lng");
                                String tempTitle = obj.getString("name");
                                String icon = obj.getString("icon");
                                String id = obj.getString("place_id");
                                String address = obj.getString("vicinity");

                                LatLng temp = new LatLng(Double.valueOf(tempLat), Double.valueOf(tempLng));

                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(temp);
                                markerOptions.title(tempTitle);

                                googleMap.addMarker(markerOptions);

                                placesFounded.add(new Place(id, tempTitle, address, icon));


                            }

                        } catch (JSONException ex) {
                            Log.d("ERRO", ex.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.getTitle();
        Toast.makeText(getContext(), marker.getTitle().toString(), Toast.LENGTH_SHORT).show();



        for (Place places : placesFounded) {

            if (places.getName().equals(marker.getTitle())) {

                String placeID = places.getId().toString();


                        //vamos para a atividade detalhes e enviamos os detalhes do place escolhido

                Intent intent = new Intent(getActivity(), PlaceDetailsActivity.class);
                intent.putExtra("placeID", placeID);

                startActivity(intent);
            }
        }

        return false;

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.setRadiusSearch) {


            Toast.makeText(getContext(), "ENTRAAAAA", Toast.LENGTH_SHORT).show();

            FragmentManager fm = getFragmentManager();
            DialogRadiusFragment setRadiusDialog = new DialogRadiusFragment();

            setRadiusDialog.setTargetFragment(this, REQUEST_CODE);

            setRadiusDialog.show(fm, "Sample Fragment");

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String newRadius = bundle.getString("radius");

                    radius = newRadius;

                    googleMap.clear();
                    placesFounded.clear();

                    addPontosProximos(latAtual, lngAtual);

                } else if (resultCode == Activity.RESULT_CANCELED) {

                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    public ArrayList<Place> getPlacesFounded(){
        return placesFounded;
    }

    //WEB SERVICE PEDIDO DOS PONTOS
    public void addPontosProximos() {


        LatLng tempLatlng = new LatLng(Double.valueOf(latAtual),Double.valueOf(lngAtual));




        String localizacaoAtual = String.valueOf(latAtual) + "," + String.valueOf(lngAtual);
        String url;

        if(Utils.TYPE != null){

            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + localizacaoAtual + "&radius=" + radius + "&type=" + Utils.TYPE + "&key=AIzaSyDmnW7cVyFiVLI9BmHX1MwhVY_xqfBruxc";

        }else{
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + localizacaoAtual + "&radius=" + radius + "&key=AIzaSyDmnW7cVyFiVLI9BmHX1MwhVY_xqfBruxc";

        }



        //String url = "http://192.168.137.1:8080/COMOOVtpfinal/googlePlaces.json";

        Log.d("TAG", url);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    public void onResponse(JSONObject response) {
                        try {

                            JSONArray arr = response.getJSONArray(Utils.param_dados);

                            for (int i = 0; i < arr.length(); i++) {

                                JSONObject obj = arr.getJSONObject(i);

                                String tempLat = obj.getJSONObject("geometry").getJSONObject("location").getString("lat");
                                String tempLng = obj.getJSONObject("geometry").getJSONObject("location").getString("lng");
                                String tempTitle = obj.getString("name");
                                String icon = obj.getString("icon");
                                String id = obj.getString("place_id");
                                String address = obj.getString("vicinity");

                                LatLng temp = new LatLng(Double.valueOf(tempLat), Double.valueOf(tempLng));

                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(temp);
                                markerOptions.title(tempTitle);

                                googleMap.addMarker(markerOptions);

                                placesFounded.add(new Place(id, tempTitle, address, icon));


                            }

                        } catch (JSONException ex) {
                            Log.d("ERRO", ex.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsObjRequest);

    }
}
