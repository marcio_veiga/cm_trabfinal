package mingos.marcio.cm_tpandroidfinal.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import mingos.marcio.cm_tpandroidfinal.R;

/**
 * Created by marcio on 18/04/2017.
 */

public class DialogRadiusFragment extends android.support.v4.app.DialogFragment {

    private TextView insertedRadius;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dialog_radius, container, false);

        getDialog().setTitle(R.string.setRadius);

        insertedRadius = (TextView) rootView.findViewById(R.id.settedRadius);


        Button setRadius = (Button) rootView.findViewById(R.id.setRadius);
        setRadius.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent()
                        .putExtra("radius",insertedRadius.getText().toString());

                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);

                dismiss();
            }
        });


        return rootView;
    }

}
