package mingos.marcio.cm_tpandroidfinal.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

import mingos.marcio.cm_tpandroidfinal.R;
import mingos.marcio.cm_tpandroidfinal.Utils;
import mingos.marcio.cm_tpandroidfinal.entities.Place;


/**
 * Created by marcio on 14/04/2017.
 */

public class TabsFragment extends Fragment {

    private static final String TAG = TabsFragment.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MapFragment places;
    ArrayList<Place> foundedPlaces;


    public TabsFragment() {}

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tabs, container, false);

        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        viewPager = (ViewPager)view.findViewById(R.id.view_pager);

        viewPager.setAdapter(new CustomFragmentPageAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setOffscreenPageLimit(1);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if(position == 1 ){
                    Toast.makeText(getContext(), "ENTRA onPage", Toast.LENGTH_SHORT).show();
                    ListPlacesFragment teste = new ListPlacesFragment();
                    teste.fillLista();
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        return view;
    }





}
