package mingos.marcio.cm_tpandroidfinal;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import mingos.marcio.cm_tpandroidfinal.adapter.MyCursorAdapter;
import mingos.marcio.cm_tpandroidfinal.db.Contrato;
import mingos.marcio.cm_tpandroidfinal.db.db;

public class FavouritePlacesActivity extends AppCompatActivity {

    db mDbHelper;
    SQLiteDatabase db;
    Cursor c, c_pessoas;
    ListView lista;
    MyCursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_places);

        mDbHelper = new db(this);
        //aceder a BD para leitura
        db = mDbHelper.getReadableDatabase();

        lista = (ListView) findViewById(R.id.listaFavoritos);
        registerForContextMenu((ListView) findViewById(R.id.listaFavoritos));
        preencheLista();



    }


    public void preencheLista(){
        getCursor();
        //query a efetuar neste caso fazemos a todas as colunas mas podemos filtrar com um novo array de colunas

        //selections -- aplicar Where's
        //Order BY podemos omitir o "Order BY"
   /*    c = db.query(false, Contrato.Pessoa.TABLE_NAME, Contrato.Pessoa.PROJECTION,
                "idade < ?", new String[]{String.valueOf(30)},
                    null, null,
                    Contrato.Pessoa.COLUMN_NOME + " ASC", null);
*/
        getCursor();
        Log.d("TAG", "TESTE");
        Log.d("TAG", c.toString());


        mAdapter = new MyCursorAdapter(FavouritePlacesActivity.this, c);

        lista.setAdapter(mAdapter);

    }


    private void refresh() {

        getCursor();

        //atualizar o adapter
        mAdapter.swapCursor(c);
    }
    //executa novamente a query select
    private void getCursor() {

        String sql = " SELECT " +
                Contrato.Local.TABLE_NAME + "." + Contrato.Local._ID + "," +
                Contrato.Local.TABLE_NAME + "." + Contrato.Local.COLUMN_NOME + "," +
                Contrato.Local.TABLE_NAME + "." + Contrato.Local.COLUMN_MORADA + " FROM " +
                Contrato.Local.TABLE_NAME + "," + Contrato.Tipo.TABLE_NAME + " WHERE " +
                Contrato.Local.TABLE_NAME + "." +  Contrato.Local._ID + "=" + Contrato.Tipo.TABLE_NAME + "." + Contrato.Tipo._ID + " AND " +
                Contrato.Tipo.TABLE_NAME + "." + Contrato.Tipo._ID + " = 1";


        //falta FITLRAR PELO ID UTILIZADOR E PELO TIPO FAVORITO


        //associar select ao cursor

        Log.d("TAG", sql);
        Log.d("TAG", String.valueOf(c));
        c = db.rawQuery(sql, null);
    }
}
