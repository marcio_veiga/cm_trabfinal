package mingos.marcio.cm_tpandroidfinal;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import mingos.marcio.cm_tpandroidfinal.db.Contrato;
import mingos.marcio.cm_tpandroidfinal.db.db;
import mingos.marcio.cm_tpandroidfinal.entities.Place;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PlaceDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Button addFavourites;
    private Button addVisited;

    private String nome;
    private String address;
    private String rating;


    private TextView Tnome;
    private TextView Tcontato;
    private TextView Tmorada;
    private TextView Trating;
    db mDbHelper;
    SQLiteDatabase db;
    String placeID;


    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = false;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            show();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_place_details);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mVisible = true;
        mContentView = findViewById(R.id.fullscreen_content);

        // Nosso codigo


        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
             placeID = (String) b.get("placeID");
        }

        mDbHelper = new db(this);
        //aceder a BD para leitura
        db = mDbHelper.getReadableDatabase();

        addFavourites = (Button)findViewById(R.id.addFavPlace);
        addVisited = (Button)findViewById(R.id.addVisitedPlace);

        addFavourites.setOnClickListener(this);
        addVisited.setOnClickListener(this);

        Tnome = (TextView)findViewById(R.id.nome);
        Tcontato = (TextView)findViewById(R.id.contacto);
        Tmorada = (TextView)findViewById(R.id.morada);
        Trating = (TextView)findViewById(R.id.rating);

        detalhesPontoAtual();

    }


    //WEB SERVICE PEDIDO DOS PONTOS
    private void detalhesPontoAtual() {

        String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid="+placeID+"&key=AIzaSyDmnW7cVyFiVLI9BmHX1MwhVY_xqfBruxc";

        Log.d("TAG", url);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    public void onResponse(JSONObject response) {
                        try {

                            JSONObject result = response.getJSONObject("result");



                               // String tempLat = obj.getJSONObject("address_components").getJSONObject("location").getString("lat");
                               // String tempLng = obj.getJSONObject("geometry").getJSONObject("location").getString("lng");
                                String tempAddress = result.getString("formatted_address");
                                String tempTitle = result.getString("name");
                               // String icon = obj.getString("icon");
                               // String id = obj.getString("id");
                               // String address = obj.getString("vicinity");

                            Tnome.setText(tempTitle);
                            Tmorada.setText(tempAddress);



                                Toast.makeText(PlaceDetailsActivity.this, tempTitle, Toast.LENGTH_SHORT).show();
                                Toast.makeText(PlaceDetailsActivity.this, tempAddress, Toast.LENGTH_SHORT).show();


                        } catch (JSONException ex) {
                            Log.d("ERRO", ex.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(PlaceDetailsActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

        MySingleton.getInstance(PlaceDetailsActivity.this).addToRequestQueue(jsObjRequest);

    }















    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.addFavPlace){
            Snackbar.make(v.getRootView(), "Adicionado aos favoritos", Snackbar.LENGTH_LONG).setAction("Action", null).show();

            ContentValues cv =  new ContentValues();

            cv.put(Contrato.Local.COLUMN_NOME, nome);
            cv.put(Contrato.Local.COLUMN_MORADA, address);
            cv.put(Contrato.Local.COLUMN_RATING, rating);
            cv.put(Contrato.Local.COLUMN_IDTIPO, 1);

            db.insert(Contrato.Local.TABLE_NAME, null, cv);

        }else if(v.getId() == R.id.addVisitedPlace){

            Snackbar.make(v.getRootView(), "Adicionado aos visitados", Snackbar.LENGTH_LONG).setAction("Action", null).show();

            ContentValues cv =  new ContentValues();

            cv.put(Contrato.Local.COLUMN_NOME, nome);
            cv.put(Contrato.Local.COLUMN_MORADA, address);
            cv.put(Contrato.Local.COLUMN_RATING, rating);
            cv.put(Contrato.Local.COLUMN_IDTIPO, 2);

            db.insert(Contrato.Local.TABLE_NAME, null, cv);
        }
    }
}
