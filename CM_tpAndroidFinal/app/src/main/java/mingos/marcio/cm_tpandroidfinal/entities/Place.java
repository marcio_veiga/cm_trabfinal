package mingos.marcio.cm_tpandroidfinal.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcio on 18/04/2017.
 */
//implementar serialziable para deste modo podermos passar um objeto de uma atividade para outra por intent
public class Place implements Serializable {

    public String id;
    public String name;
    public String formatted_address;
    public String icon;


    public Place(String id, String name, String formatted_address, String icon) {
        this.id = id;
        this.name = name;
        this.formatted_address = formatted_address;
        this.icon = icon;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public String getIcon() {
        return icon;
    }
}
