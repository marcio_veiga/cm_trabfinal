package mingos.marcio.cm_tpandroidfinal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import mingos.marcio.cm_tpandroidfinal.fragments.MapFragment;
import mingos.marcio.cm_tpandroidfinal.fragments.TabsFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public TextView navHeaderEmail;
    public TextView navHeaderName;



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        //inicializar sharedPreferences
        android.content.SharedPreferences sharedPref = getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
        String email = sharedPref.getString(Utils.EMAIL, null);
        String nome = sharedPref.getString(Utils.NOME, null);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, new TabsFragment())
                .commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        navHeaderEmail = (TextView) headerView.findViewById(R.id.navHeaderEmail);
        navHeaderName = (TextView) headerView.findViewById(R.id.navHeaderNome);

        navHeaderEmail.setText(email);
        navHeaderName.setText(nome);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_everything:
                Utils.TYPE = null;
                break;
            case R.id.nav_historicalMonuments:
                Utils.TYPE = "church";

                break;
            case R.id.nav_artMuseums:
                Utils.TYPE = "art_gallery";

                break;
            case R.id.nav_parks:
                Utils.TYPE = "amusement_park";

                break;
            case R.id.nav_foodDrink:
                Utils.TYPE = "bakery";

                break;
            case R.id.nav_architecture:
                Utils.TYPE = "museum";

                break;
            case R.id.nav_favPlaces:

               Intent favPlaces = new Intent(this, FavouritePlacesActivity.class);
                startActivity(favPlaces);

                break;
            case R.id.nav_visitPlaces:

               // Intent visitPlaces = new Intent(this, VisitedActivity.class);
              //  startActivity(visitPlaces);

                break;
            case R.id.nav_settings:

              //  Intent settingsAct = new Intent(this, VisitedActivity.class);
              //  startActivity(settingsAct);

                break;
            case R.id.nav_logout:
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.shared_pref_1), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove("email");
                editor.commit();


                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            default:

        }

        MapFragment teste = new MapFragment();
        teste.addPontosProximos();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
